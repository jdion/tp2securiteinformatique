﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;
using Server.Service;



namespace TravailPratique2
{
    class Server
    {
        static void Main(string[] args)
        {
            if (args.Length != 0)
            {

                TimeSpan t = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc));
                double unixTime = t.TotalSeconds;

                if (String.Compare(args[0], "serveur") == 0)
                {
                    string[] items = args[1].Split('#');

                    string idClient = items[0], addressClient = items[1], idV = items[2], ticketV = items[3];

                    ClientTable ct = new ClientTable("cles");
                    Encryption.Key = ct.GetClientKey(idV);

                    char[] trimChars = { '\r', '\n' };
                    ticketV = ticketV.TrimEnd(trimChars);
                    ticketV = Encryption.Decode(ticketV);

                    string[] ticketItems = ticketV.Split('#');

                    if (String.Compare(ticketItems[0], idClient) != 0)
                    {
                        Console.Out.WriteLine("Client id is not matching");
                        Console.In.ReadLine();
                        return;
                    }

                    else if (String.Compare(ticketItems[1], addressClient) != 0)
                    {
                        Console.Out.WriteLine("Client address is not matching");
                        Console.In.ReadLine();
                        return;
                    }

                    else if (String.Compare(ticketItems[2], idV) != 0)
                    {
                        Console.Out.WriteLine("Server id not matching");
                        Console.In.ReadLine();
                        return;
                    }
                    else
                    {

                        Int32 time; bool timeParsed = Int32.TryParse(ticketItems[3], out time);
                        Int32 duration; bool durationParsed = Int32.TryParse(ticketItems[4], out duration);

                        if (!timeParsed || !durationParsed)
                        {
                            Console.Out.WriteLine("Invalid time format");
                            Console.In.ReadLine();
                            return;
                        }

                        if ((Int32)unixTime > time + duration)
                        {
                            Console.Out.WriteLine("Ticket is expired");
                            Console.In.ReadLine();
                            return;
                        }
                    }

                    Console.Out.WriteLine("OK");

                    //Démarrage du service :).
                    Arkanoid app = new Arkanoid();
                    app.Start();
                }
            }
        }
    }

}
