﻿using SFML.Audio;
using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    namespace Service
    {

        public class Arkanoid
        {
            public const int windowWidth = 800, windowHeight = 600;
            public const int countBlocksX = 11, countBlocksY = 4;

            RenderWindow window;
            //float frametime = 0.0f;

            Vector2f lastMousePosition = new Vector2f(0, 0);
            Vector2f currentMousePosition = new Vector2f(0, 0);

            public bool isIntersecting(Entity mA, Entity mB)
            {
                return mA.right >= mB.left && mA.left <= mB.right
                              && mA.bottom >= mB.top && mA.top <= mB.bottom;
            }

            public void testCollision(Paddle paddle, Ball ball)
            {
                if (!isIntersecting(paddle, ball)) return;

                ball.velocity.Y = -ball.ballVelocity;
                if (ball.x < paddle.x) ball.velocity.Y = -ball.ballVelocity;
                else ball.velocity.X = ball.ballVelocity;
            }

            public void testCollision(Brick brick, Ball ball)
            {
                if(!isIntersecting(brick, ball)) return;
                brick.Destroyed = true;

                float overlapLeft   =   ball.right - brick.left;
                float overlapRight  =   brick.right - ball.left;
                float overlapTop    =   ball.bottom - brick.top;
                float overlapBottom =   brick.bottom - ball.top;

                bool ballFromLeft = Math.Abs(overlapLeft) < Math.Abs(overlapRight);
                bool ballFromTop  = Math.Abs(overlapTop) < Math.Abs(overlapBottom);

                float minOverlapX = ballFromLeft ? overlapLeft : overlapRight;
                float minOverlapY = ballFromTop ? overlapTop : overlapBottom;

                if (Math.Abs(minOverlapX) < Math.Abs(minOverlapY))
                    ball.velocity.X = ballFromLeft ? -ball.ballVelocity : ball.ballVelocity;
                else
                    ball.velocity.Y = ballFromTop ? -ball.ballVelocity : ball.ballVelocity;        
            }


            public void Start()
            {

                window = new RenderWindow(new VideoMode(windowWidth, windowHeight), "Arkanoid");
                window.SetVisible(true);
                window.Closed += new EventHandler(OnClosed);
                window.KeyPressed += new EventHandler<KeyEventArgs>(OnKeyPressed);
                window.MouseButtonPressed += new EventHandler<MouseButtonEventArgs>(OnMousePressed);
                window.MouseButtonReleased += new EventHandler<MouseButtonEventArgs>(OnMouseReleased);
                window.MouseMoved += new EventHandler<MouseMoveEventArgs>(OnMouseMoved);

                Text text = new Text();
                text.DisplayedString = "YOU WON";
                text.Scale = new Vector2f(4, 4);
                text.Origin = new Vector2f((float)text.Scale.X / 2, (float)text.Scale.Y / 2);
                text.Position = new Vector2f(50, 250);

                string pathOfExecutingAssembly = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                Font f = new Font( pathOfExecutingAssembly+ "\\comic.ttf");
                text.Font = f;
                
                text.Style = Text.Styles.Bold;

                Stopwatch stopWatch = new Stopwatch();
                long tickLastTime = 0;
                long tickThisTime = 0;

                float dt = 0.016667f;
                float accumulator = 0.0f;
                float t = 0.0f;
                float elapsedTime = 0.0f;

                Ball ball = new Ball(windowWidth / 2, windowHeight / 2);
                Paddle paddle = new Paddle(windowWidth / 2, windowHeight - 50);
                List<Brick> bricks = new List<Brick>();
                Random r = new Random();

                for(int i = 0; i < countBlocksX; ++i)        
                    for(int j= 0; j < countBlocksY; ++j)
                        bricks.Add(new Brick((i + 1) * (Brick.blockWidth + 3) + 22, (j + 2) * (Brick.blockHeight + 3), r.Next(0,5)));


                while (window.IsOpen())
                {
                    stopWatch = Stopwatch.StartNew();
                    elapsedTime = (float)(tickThisTime) / Stopwatch.Frequency;

                    string str = String.Format("{0} {1}", 1.0f / elapsedTime, elapsedTime);

                    accumulator += elapsedTime;

                    while (accumulator >= dt)
                    {
                        window.DispatchEvents();

                        ball.Update(elapsedTime);
                        paddle.Update(elapsedTime);

                        testCollision(paddle, ball);
                        foreach (Brick brick in bricks)
                        {
                            testCollision(brick, ball);

                        }

                        foreach (Brick brick in bricks.ToList<Brick>())
                        {
                            if (brick.Destroyed)
                                bricks.Remove(brick);
                        }

                        accumulator -= dt;
                        t += dt;
                        //Console.WriteLine("Frame per second / Frametime: " + str);
                    }

                    window.Clear(Color.White);


                    window.Draw(ball.Shape);
                    window.Draw(paddle.Shape);
                    foreach (Brick brick in bricks) window.Draw(brick.Shape);


                    if (bricks.Count == 0)
                    {
                        text.Color = Entity.ChooseColor(r.Next(5));
                        window.Draw(text);
                    }

                    window.Display();

                    stopWatch.Stop();
                    tickLastTime = tickThisTime;
                    tickThisTime = stopWatch.ElapsedTicks;
                }


            }

            void OnClosed(object sender, EventArgs e)
            {
                window.Close();
            }

            void OnMouseMoved(object sender, MouseMoveEventArgs e)
            {
                lastMousePosition = currentMousePosition;
                currentMousePosition = new Vector2f(e.X, e.Y);
                Vector2f v = window.MapPixelToCoords(new Vector2i(e.X, e.Y));
            }

            void OnMouseReleased(object sender, MouseButtonEventArgs e)
            {

            }

            void OnMousePressed(object sender, MouseButtonEventArgs e)
            {

            }

            void OnKeyPressed(object sender, KeyEventArgs e)
            {
                Window window = (Window)sender;
                if (e.Code == Keyboard.Key.Escape)
                    window.Close();
            }
        }

        abstract public class Entity
        {

            public abstract float x { get; }
            public abstract float y { get; }
            public abstract float left { get; }
            public abstract float right { get; }
            public abstract float top { get; }
            public abstract float bottom { get; }

            public abstract void Update(float delta);

            static public Color ChooseColor(int value)
            {
                switch (value)
                {
                    case 0: return Color.Blue;
                    case 1: return Color.Red;
                    case 2: return Color.Yellow;
                    case 3: return Color.Magenta;
                    case 4: return Color.Cyan;
                }

                return Color.Black;
            }

        }


        public class Ball : Entity
        {
            public CircleShape shape;

            private float ballRadius = 10.0f;
            public float ballVelocity = 8.0f;
            public Vector2f velocity;

            public override float x { get { return shape.Position.X; } }
            public override float y { get { return shape.Position.Y; } }
            public override float left { get { return x - shape.Radius; } }
            public override float right { get { return x + shape.Radius; } }
            public override float top { get { return y - shape.Radius; } }
            public override float bottom { get { return y + shape.Radius; } }

            public Drawable Shape { get { return shape; } }

            public Ball(float mX, float mY)
            {
                    shape = new CircleShape();
                    velocity = new Vector2f(-ballVelocity, -ballVelocity);
                    shape.Position = new Vector2f(mX, mY);
                    shape.Radius = ballRadius;
                    shape.FillColor = Color.Red;
                    shape.OutlineColor = Color.Black;
                    shape.OutlineThickness = 1.0f;
                    shape.Origin = new Vector2f(ballRadius, ballRadius);
            }

            public override void Update(float delta) 
            {
                    shape.Position = shape.Position + velocity;

                    if(left < 0) velocity.X = ballVelocity;
                    else if(right > Arkanoid.windowWidth) velocity.X = -ballVelocity;

                    if(top < 0) velocity.Y = ballVelocity;
                    else if (bottom > Arkanoid.windowHeight) velocity.Y = -ballVelocity;
            }


          }




        public class Paddle :  Entity
        {
                private RectangleShape shape;
                private Vector2f velocity;
                private float paddleVelocity = 10.0f;
                private float paddleWidth = 60.0f, paddleHeight = 20.0f;

                public override  float x      { get {return shape.Position.X;}    }
                public override  float y      { get {return shape.Position.Y;}    }
                public override  float left   { get {return x - shape.Size.X / 2.0f;}   }
                public override  float right  { get {return x + shape.Size.X / 2.0f;}   }
                public override  float top    { get {return y - shape.Size.Y / 2.0f;}   }
                public override  float bottom { get {return y + shape.Size.Y / 2.0f;}   }

                public Drawable Shape { get { return shape; } }


                public  Paddle(float mX, float mY) 
                {
                        shape = new RectangleShape();
                        velocity = new Vector2f(0,0);
                        shape.Position = new Vector2f(mX, mY);
                        shape.Size = new Vector2f(paddleWidth, paddleHeight);
                        shape.FillColor = Color.Black;
                        shape.Origin = new Vector2f(paddleWidth / 2.0f, paddleHeight / 2.0f);
                }

                public override  void Update(float delta)
                { 
                        shape.Position = shape.Position + velocity;
                
                         if (Keyboard.IsKeyPressed(Keyboard.Key.A) && left > 0)
                            velocity.X = -paddleVelocity;
                         else if (Keyboard.IsKeyPressed(Keyboard.Key.D) && right < Arkanoid.windowWidth)
                            velocity.X = paddleVelocity;
                         else
                            velocity.X = 0;
                }

        }


        public class Brick : Entity
        {

            private RectangleShape shape;
            private bool destroyed = false;
            public static float blockWidth = 60.0f, blockHeight = 20.0f;

            public override  float x { get { return shape.Position.X; } }
            public override  float y { get { return shape.Position.Y; } }
            public override  float left { get { return x - shape.Size.X / 2.0f; } }
            public override  float right { get { return x + shape.Size.X / 2.0f; } }
            public override  float top { get { return y - shape.Size.Y / 2.0f; } }
            public override  float bottom { get { return y + shape.Size.Y / 2.0f; } }

            public bool Destroyed { get { return destroyed; } set { destroyed = value; } }
            public Drawable Shape { get { return shape; } }

            public Brick(float mX, float mY, int color) 
            {
               
                shape = new RectangleShape();
                shape.Position = new Vector2f(mX, mY);
                shape.Size = new Vector2f(blockWidth, blockHeight);
                shape.Origin = new Vector2f(blockWidth / 2.0f, blockHeight / 2.0f);
                shape.FillColor = ChooseColor(color);
                shape.OutlineColor = Color.Black;
                shape.OutlineThickness = 1.0f;
            }

            public override  void Update(float delta)
            {

            }


        }

    }


}

