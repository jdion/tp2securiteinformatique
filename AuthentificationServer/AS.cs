﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;
using System.IO;


namespace TravailPratique2
{
    class AuthentificationServer
    {
        static void Main(string[] args)
        {
            if (args.Length != 0)
            {
                TimeSpan t = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc));
                double unixTime = t.TotalSeconds;

                if (String.Compare(args[0], "as") == 0)
                {
                    string[] items = args[1].Split('#');

                    string  idClient        = items[0],
                            addressClient   = items[1],
                            idTgs           = items[2];

                    string timestamp = ((long)unixTime).ToString();
                    string span = "3600";
                    string s = "#";

                    string spanExpired = "0";

                    ClientTable ct = new ClientTable("cles");
                    Encryption.Key = ct.GetClientKey(idTgs);

                    string ticketTgs = Encryption.Encode(idClient + s + addressClient + s + idTgs + s + timestamp + s + span);
                    string ticketTgsExpired = Encryption.Encode(idClient + s + addressClient + s + idTgs + s + timestamp + s + spanExpired);
                    string ticketTgsModified= Encryption.Encode(idClient + s + "A#$" + addressClient + s + idTgs + s + timestamp + s + span);
                    Log.Out("ticketTgs", ticketTgs);

                    Log.Out("CommandeI", "tgt Sylvain#1.2.3.4#V1#\"" + ticketTgs + "\"");

                    Log.Out("CommandeII", "tgt Sylvain#1.2.2.4#V1#\"" + ticketTgs + "\"");

                    Log.Out("CommandeIV", "tgt Sylvain#1.2.2.4#V1#\"" + ticketTgsExpired + "\"");

                    Log.Out("CommandeV", "tgt Sylvain#1.2.2.4#V1#\"" + ticketTgsModified + "\"");

                    Console.WriteLine("Ticket pour le tgs: " + ticketTgs);
                }
            }

        }
        
    }
}
