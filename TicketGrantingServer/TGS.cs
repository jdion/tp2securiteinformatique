﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

namespace TravailPratique2
{
    class TicketGrantingServer
    {
        static void Main(string[] args)
        {

            if (args.Length != 0)
            {
            //Ktgs[IDC ADC IDtgs TS1 Duree1
            //IDC#ADC#IDV #Tickettgs
            //Ticket V = E(KV , [ID C ADC IDV TS2 Duree2 ])

            TimeSpan t = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc));
            double unixTime = t.TotalSeconds;

            if (String.Compare(args[0], "tgt") == 0)
            {
                string[] items = args[1].Split('#');

                string idClient = items[0],
                       addressClient = items[1],
                       idV = items[2],
                       ticketTgs = items[3];



                string idTgs = "TGS1";
                char[] trimChars = { '\r', '\n' };
                ticketTgs = ticketTgs.TrimEnd(trimChars);
                ClientTable ct = new ClientTable("cles");
                Encryption.Key = ct.GetClientKey(idTgs);

                ticketTgs = Encryption.Decode(ticketTgs);

                string[] ticketItems = ticketTgs.Split('#');

                if (String.Compare(ticketItems[0], idClient) != 0)
                {
                    Console.Out.WriteLine("Client id is not matching");
                    return;
                }

                else if (String.Compare(ticketItems[1], addressClient) != 0)
                {
                    Console.Out.WriteLine("Client address is not matching");
                    return;
                }

                else if (String.Compare(ticketItems[2], idTgs) != 0)
                {
                    Console.Out.WriteLine("Ticket Granting Server id not matching");
                    return;
                }

                else
                {

                    Int32 time; bool timeParsed = Int32.TryParse(ticketItems[3], out time);
                    Int32 duration; bool durationParsed = Int32.TryParse(ticketItems[4], out duration);

                    if (!timeParsed || !durationParsed)
                    {
                        Console.Out.WriteLine("Invalid time format");
                        return;
                    }


                    if ((Int32)unixTime > time + duration)
                    {
                        Console.Out.WriteLine("Ticket is expired");
                        return;
                    }
                }

                string timestamp = ((Int32)unixTime).ToString();
                string span = "3600";
                string s = "#";
                string spanExpired = "3600";

                Encryption.Key = ct.GetClientKey(idV);
                string ticketV = Encryption.Encode(idClient + s + addressClient + s + idV + s + timestamp + s + span);
                string ticketVExpired = Encryption.Encode(idClient + s + addressClient + s + idV + s + timestamp + s + spanExpired);
                string ticketVModified = Encryption.Encode(idClient + s + addressClient + s + idV + s + timestamp + s + span);
                Console.Out.WriteLine("Ticket pour le serveur: " + ticketV);
                Log.Out("ticketV", ticketV);

                Log.Out("CommandeI", "serveur Sylvain#1.2.3.4#V1#\"" + ticketV + "\"");

                Log.Out("CommandeIII", "serveur Sylvain#1.2.2.4#V1#\"" + ticketV + "\"");

                Log.Out("CommandeIV", "serveur Sylvain#1.2.2.4#V1#\"" + ticketVExpired + "\"");

                Log.Out("CommandeV", "serveur Sylvain#1.2.2.4#V1#\"" + ticketVModified + "\"");

            }

            }
        }
    }
}
