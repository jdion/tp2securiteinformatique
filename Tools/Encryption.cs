﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tools
{
    public class Encryption
    {
        static string key = "default";

        static public string Key
        {
            set { key = value; }

            get { if (key == null) return ""; else return key; }
        }

        static public String Mask(int length)
        {
            if (key == null)
                throw new System.ArgumentNullException("key", "Parameter key cannot be null");

            StringBuilder repeatedKey = new StringBuilder();

            for (int i = 0; i < length; ++i)
            {
                repeatedKey.Append(key[i % key.Length]);
            }

            return repeatedKey.ToString();
        }

        static public String Encode(String message)
        {
            StringBuilder encodedMessage = new StringBuilder();
            String mask = Mask(message.Length);

            for (int i = 0; i < message.Length; ++i)
            {
                int mm = (int)message[i] + (int)mask[i];
                char e;
                //((val-min)%(max-min+1) + (max-min+1)) % (max-min+1) + min
                e = (char)(((mm - 33) % (122 - 33 + 1) + (122 - 33 + 1)) % (122 - 33 + 1) + 33);
                if (e == 62) e = (char)123;
                encodedMessage.Append(e);
            }

            return encodedMessage.ToString();
        }

        static public String Decode(String message)
        {
            StringBuilder decodedMessage = new StringBuilder();
            String mask = Mask(message.Length);
            String msg = message.Replace((char)123, (char)62);

            for (int i = 0; i < msg.Length; ++i)
            {
                int mm = (int)msg[i] - (int)mask[i];
                char e;

                e = (char)(((mm - 33) % (122 - 33 + 1) + (122 - 33 + 1)) % (122 - 33 + 1) + 33);
                
                decodedMessage.Append(e);
            }

            return decodedMessage.ToString();
        }
    }
}
