﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Tools
{
    public class ClientTable
    {
        Dictionary<string, string> table;

        public string GetClientKey(string identifier)
        {
            return table[identifier]; 
        }

        public void PrintTable()
        {
            foreach (KeyValuePair<string,string> kv in table)
            {
                Console.WriteLine("Key: " + kv.Key + " Value: " + kv.Value) ;
            }
        }

        public ClientTable()
        {
            table = new Dictionary<string, string>();
        }

        public ClientTable(string filename)
        {
            table = new Dictionary<string, string>();
            LoadDataFromTextFile(filename);
        }

        public void LoadDataFromTextFile(string filename)
        {
            string pathOfExecutingAssembly = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);


            string path = pathOfExecutingAssembly + "\\" + filename;
            path = Path.ChangeExtension(path, ".txt");
            //Console.WriteLine(filename);

            if (File.Exists(path))
            {

                StreamReader reader = File.OpenText(path);
                //Console.WriteLine("Opening file: " + path);

                string line;

                try
                {
                    while ((line = reader.ReadLine()) != null)
                    {
                        string[] items = line.Split(' ');
                        String identifier = items[0];
                        String secretKey = items[1];

                        table.Add(identifier, secretKey);

                    }
                }
                catch (System.IO.IOException e)
                {
                    Console.WriteLine("Error reading from {0}. Message = {1}", path, e.Message);
                }
                finally
                {
                    if (reader != null)
                    {
                        reader.Close();
                    }
                }

                //Console.WriteLine(filename + " has succesfully loaded");

            }
            else
            {
                //Console.WriteLine("File: " + filename + " does not exist.");
            }
        }
    }
}
