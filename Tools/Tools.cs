﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace Tools
{
    class Tools
    {
        static void Main(string[] args)
        {

            test(33, 64);
            test(64, 96);
            test(96, 123);
            System.Console.ReadKey();

        }

        static void test(int min, int max)
        {
            StringBuilder str = new StringBuilder();
            for (int i = min; i < max; i++)
                str.Append((char)i);

            Encryption.Key = "ab5utiqp";
            string e = Encryption.Encode(str.ToString());
            string d = Encryption.Decode(e);

            System.Console.Out.WriteLine("Original: " + str.ToString());
            System.Console.Out.WriteLine("Encode: " + e);
            System.Console.Out.WriteLine("Decode: " + d);
        }


    }

    public class Log
    {

        static public void Out(string filename, string message)
        {
            string pathOfExecutingAssembly = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string path = pathOfExecutingAssembly + "\\" + filename;
            path = Path.ChangeExtension(path, ".txt");

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(path, true))
            {

                file.WriteLine(message);
            }
        }
    }
}
